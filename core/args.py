import argparse
import sys

help_help = "Afficher l'aide"
help_userfile = """
    Spécifie l'emplacement du fichier source (non pris en compte si -va est
        utilisé)
    """
help_name = "Spécifie un nom de fichier de sortie"
help_start = "Spécifie l'heure de début (8 par défaut)"
help_end = "Spécifie l'heure de fin (20 par défaut)"
help_zoom = "Spécifie la puissance du zoom (0.7 par défaut)"
help_week = "Permet d'ajouter ou non des jours dans la semaine"
help_pdf = "Désactive la génération du fichier pdf"
help_tex = "Désactive la génération du fichier latex"
help_version = "Donne la version actuelle du programme"
help_variable = "Permet de faire passer une variable de texte"
help_output = "Spécifie un dossier de sortie"
help_json = "Active la sortie json (désactive la sorties tex et pdf)"
help_semestre = "Récupère le  semestre (désactive la sorties tex et pdf)"

# Default variable list
document_name = "Emplois du temps"
hour_start = '8'
hour_end = '20'
yscale = '0.7'
week = 'Lundi, Mardi, Mercredi, Jeudi, Vendredi, Samedi'
output = "compilation"

generate_pdf = True
generate_tex = True

output_json = False
output_semestre = False


version = "EDT converter : 2.0"

# To parse command line arguments

arg_parser = argparse.ArgumentParser(description="Fonctionnement")

arg_parser.add_argument("user_file", action="store", nargs="+",
                        help=help_userfile)
arg_parser.add_argument("-n", "--name", action="store", type=str,
                        default=document_name, help=help_name)
arg_parser.add_argument("-s", "--start", action="store", type=str,
                        default=hour_start, help=help_start)
arg_parser.add_argument("-e", "--end", action="store", type=str,
                        default=hour_end, help=help_end)
arg_parser.add_argument("-z", "--zoom", action="store", type=str,
                        default=yscale, help=help_zoom)
arg_parser.add_argument("-w", "--week", action="store", type=str,
                        default=week, help=help_week)
arg_parser.add_argument("-p", "--pdf", action="store_false",
                        default=generate_pdf, help=help_pdf)
arg_parser.add_argument("-t", "--tex", action="store_false",
                        default=generate_tex, help=help_tex)
arg_parser.add_argument("-va", "--variable", action="store", type=str,
                        default=None, help=help_variable)
arg_parser.add_argument("-o", "--output", action="store", type=str,
                        default=output, help=help_output)
arg_parser.add_argument("-j", "--json", action="store_true",
                        default=output_json, help=help_json)
arg_parser.add_argument("-se", "--semestre", action="store_true",
                        default=output_semestre, help=help_semestre)
arg_parser.add_argument("-v", '--version', action='version', version=version,
                        help=help_version)

namespace = arg_parser.parse_args(sys.argv)
result = arg_parser.parse_args()
arguments = dict(result._get_kwargs())
