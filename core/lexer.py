import ply.lex as lex
import re

# List of token names.
tokens = (
    'UV',
    'TYPE',
    'START',
    'END',
    'ROOM',
    'DAY',
    'TITLE',
    'AND',
    'HALF',
    'EXT',
)

# Regular expression rules
t_UV = r'(?<=\n)([A-Z0-9]+)(\s)'
t_TITLE = r'^(.+)'
t_TYPE = r'[CT][DP\s][0-9]'
t_ROOM = r'(?<=(en\s))([A-Z0-9]+)'
t_DAY = r'(?<=\s)[LMJVS]([AE])?(?=\s)'
t_AND = r'(?<=\s)(et)(?=\s)'
t_EXT = r'(?<=\s)(HORS\sEMPLOI\sdu\sTEMPS)(?=\s)'


# Regular expression rules with some action code
def t_START(t):
    r'[0-9]([0-9])?(H)[0-9]{2}(?=\-)'

    t.value = convert_hour(t.value)

    return t


def t_END(t):
    r'(?<=\-)[0-9]([0-9])?(H)[0-9]{2}'

    t.value = convert_hour(t.value)

    return t


def t_HALF(t):
    r'(?<=\s)(\(1\sSEMAINE\s/\s2\))(?=\s)'
    t.value = 'rectleft'
    return t


def convert_hour(value):
    """
    Convert hour from string to decimal hour
    """
    # Get hour and delete 'H'
    hour = re.search(r'[0-9]([0-9])?(H)', value)
    hour = float(re.sub(r'(H)', '', hour.group(0)))

    # Get minits, delete 'H' and convert to decimals minutes
    minutes = re.search(r'(H)[0-9]{2}', value)
    minutes = float(re.sub(r'(H)', '', minutes.group(0))) / 60

    final = hour + minutes

    return final

# Error handling rule


def t_error(t):
    t.lexer.skip(1)

# Define a rule so we can track line numbers


def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

analyzer = lex.lex()
