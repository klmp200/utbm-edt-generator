#!/usr/bin/env python3

from core.parser import parse
from core.args import arguments
import os
import shutil
import json
import re
from pylatex import Document, Package
from pylatex.package import Command
from pylatex.command import Options
from pylatex.base_classes import BaseLaTeXNamedContainer


# Variable list
document_name = arguments['name']
hour_start = arguments['start']
hour_end = arguments['end']
yscale = arguments['zoom']
week = arguments['week']

generate_pdf = arguments['pdf']
generate_tex = arguments['tex']

output = arguments['output']

json_o = arguments['json']
semestre = arguments['semestre']


# Fonction main


def main(user_file, document_name=document_name, start=hour_start,
         end=hour_end, zoom=yscale, week=week,
         output=output, json_out=json_o, semestre=semestre,
         pdf=generate_pdf, tex=generate_tex, variable=arguments['variable']):

    # Get file to analyse and feed lexer with
    if variable is None:
        try:
            data = open(user_file, 'r').read()
        except ValueError:
            print("Le fichier présenté n'existe pas ou présente des erreures")
    else:
        data = variable

    events, title, events_ext = parse(data)

    output = os.path.abspath(output)

    if json_out or semestre:
        if json_out:
            new_json = json.dumps({'TITLE': title, 'EVENTS': events,
                                   'EVENTS_EXT': events_ext}, indent=4,
                                  separators=(',', ': '))
            json_file = open(output + '/' + document_name + '.json', 'w')
            json_file.write(new_json)
            json_file.close()

        if semestre:
            periode = re.findall(r'^([A-Z]+)', title)
            annee = re.findall(r'(?<=\s)([0-9]+)', title)
            semestre = '_'.join(periode + annee)
            print(semestre)

    else:
        shutil.copyfile('pas-edt.sty', '/tmp/pas-edt.sty')

        os.chdir('/tmp')

        doc = Document(title=title, maketitle=False)
        doc.packages.append(Package('pas-edt'))

        fill_document(doc, events, events_ext)

        if generate_pdf:
            doc.generate_pdf(document_name, clean=True)
            shutil.copyfile(document_name + '.pdf', output + '/'
                            + document_name + '.pdf')
        if generate_tex:
            doc.generate_tex(document_name)
            shutil.copyfile(document_name + '.tex', output + '/'
                            + document_name + '.tex')
        os.system('rm -f ' + document_name + '*')
        os.remove('pas-edt.sty')


class Container(BaseLaTeXNamedContainer):

    """
    A class representing a custom LaTeX environment.
    This class represents a custom LaTeX environment named
    ``tikzpicture`` because basic TikZ's options can't be changed.
    """

    container_name = 'tikzpicture'

    def __init__(self, options=None):
        packages = [Package('mdframed')]

        super().__init__(options=options,
                         packages=packages)


def day_to_number(day):
    if day == 'L':
        number = 1
    elif day == 'MA':
        number = 2
    elif day == 'ME':
        number = 3
    elif day == 'J':
        number = 4
    elif day == 'V':
        number = 5
    elif day == 'S':
        number = 6
    else:
        number = 7
    return number


def fill_document(doc, events, events_ext):
    """
        This is where we generate all the document
    """

    doc.append(Command('jours', arguments=week))

    with doc.create(Container(options='yscale=' + yscale)):
        doc.append(Command('planning',
                           options=Options('demiheures', 'quartheures',
                                           'start=' + hour_start,
                                           'end=' + hour_end)))

        for event in events:
            if 'HALF' not in event.keys():
                event['HALF'] = ''
            doc.append(Command('creneau',
                               options=Options('day=' +
                                               str(day_to_number(event['DAY'])
                                                   ),
                                               'start=' + str(event['START']),
                                               'end=' + str(event['END']),
                                               'week=' + event['HALF']),
                               arguments=(event['UV'] + event['TYPE'] + ' ' +
                                          event['ROOM'])))
    for event_ext in events_ext:
        doc.append(event_ext['UV'] + ' : hors emplois du temps')

if __name__ == '__main__':
    main(arguments['user_file'][0])
