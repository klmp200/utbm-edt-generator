# EDT-GENERATOR

Ceci est un petit programme qui transforme les horribles emplois du temps fournis par l'UTBM en de sublime emplois du temps pdf.

Ce programme nécessite l'installation de python 3.4 et des dépendances disponnibles dans le fichier requirements.txt ainsi que pdflatex pour la compilation du fichier latex (nécessite les modules tikz, xkeyval et multido)

Dépendances Ubuntu:
  * texlive-latex-extra
  * texlive-extra-utils
  * texlive-generic-recommended
  * texlive-fonts-recommended

APT fera le reste!
